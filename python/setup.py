import os
import setuptools

build = os.getenv("CI_PIPELINE_IID")
with open(os.path.join("..", "config", "base_version.txt")) as f:
    base_version = f.read()

grpc_versions = {}
with open(os.path.join("..", "config", "grpc_version.txt")) as f:
    for line in f:
        name, var = line.partition("=")[::2]
        grpc_versions[name.strip()] = var

protobuf_versions = {}
with open(os.path.join("..", "config", "protobuf_version.txt")) as f:
    for line in f:
        name, var = line.partition("=")[::2]
        protobuf_versions[name.strip()] = var

version = base_version
if build:
    version += "." + build

setuptools.setup(
    name="proto-model",
    version=version,
    author="Lukas Deichmann, David Gregorczyk",
    author_email="lukasdeichmann@gmail.com",
    zip_safe=True,
    url="https://gitlab.com/sdc-suite/protosdc/proto-model",
    package_dir={"": "src"},
    packages=setuptools.find_namespace_packages(where="src"),
    install_requires=[
        "protobuf == " + protobuf_versions["protobuf_python_version"],
        "grpcio == " + grpc_versions["grpc_python_version"]
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3 :: Only"
    ],
    python_requires='>=3.6',
)
