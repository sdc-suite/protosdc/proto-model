rootProject.name = "proto-model"

pluginManagement {
    includeBuild("build-logic")

    repositories {
        gradlePluginPortal()
        maven("https://gitlab.com/api/v4/projects/60445622/packages/maven")
    }
}

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include("java-proto")
include("kotlin-proto")
include("kotlin-model")
include("proto-kotlin-mapper")
include("xml-kotlin-mapper")

// rename the subprojects
project(":kotlin-model").name = "biceps-model-kt"
project(":kotlin-proto").name = "protosdc-model-kt"
project(":java-proto").name = "protosdc-model-java"
