package org.somda.protosdc.proto.mapping

interface BicepsExtensionsProtoHandler {
    fun namespace(): String
    fun mapKnownToKotlin(data: com.google.protobuf.Any): kotlin.Any?
    fun mapKnownToProto(data: kotlin.Any): com.google.protobuf.Any?
}