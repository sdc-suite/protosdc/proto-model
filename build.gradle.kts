val protoConverter: Configuration by configurations.creating

plugins {
    id("org.somda.sdc.protosdc_model.shared")
    id(libs.plugins.org.somda.gitlab.maven.publishing.get().pluginId) version libs.versions.somda.plugins.get()
}

val protoModelSourceDir = "proto"

val javaProtoModel = "java-proto/src/main/proto"
val kotlinProtoModel = "kotlin-proto/src/main/proto"
val rustProtoModel = "rust/protosdc-proto/src/proto"
val kotlinModel = "kotlin-model/src/main/kotlin/org"
val kotlinMapperModel = "proto-kotlin-mapper/src/main/kotlin/org"

val rustBiceps = listOf(
    "rust/protosdc-biceps/src/biceps",
    "rust/protosdc-biceps/src/extensions",
    "rust/protosdc-biceps/src/xml"
)

dependencies {
    protoConverter(libs.protosdc.converter)
}

configure<JavaPluginExtension> {
    sourceCompatibility = JavaVersion.VERSION_17
}

val schemaFolder = "./xml_schema"

val executeConverterTask = task("executeConverter", JavaExec::class) {
    mainClass.set("org.somda.protosdc_converter.converter.MainKt")
    classpath = protoConverter
    args = listOf(
        "--config", "converter_config.toml"
    )

    // always clean any previous proto before generating new output
    dependsOn(tasks.clean)
}

val executeConverterExtensionsTask = task("executeConverterExtensions", JavaExec::class) {
    mainClass.set("org.somda.protosdc_converter.converter.MainKt")
    classpath = protoConverter
    args = listOf("--config", "converter_config_sdpi.toml")

    dependsOn(executeConverterTask)
    mustRunAfter(executeConverterTask)
}

val copyProtoToJava = tasks.create<Copy>("copyProtoToJava") {
    from(protoModelSourceDir)
    into(javaProtoModel)

    mustRunAfter(executeConverterExtensionsTask, executeConverterTask)
}

val copyProtoToKotlin = tasks.create<Copy>("copyProtoToKotlin") {
    from(protoModelSourceDir)
    into(kotlinProtoModel)

    mustRunAfter(executeConverterExtensionsTask, executeConverterTask)
}

val copyProtoToRust = tasks.create<Copy>("copyProtoToRust") {
    from(protoModelSourceDir)
    into(rustProtoModel)

    mustRunAfter(executeConverterExtensionsTask, executeConverterTask)
}


val copyTask = tasks.create("copyProto") {
    dependsOn(copyProtoToJava, copyProtoToKotlin, copyProtoToRust)
}

val cleanGenerated = tasks.create<Delete>("cleanGenerated") {
    // remove all generated output
    listOf(javaProtoModel, kotlinProtoModel, rustProtoModel, kotlinModel, kotlinMapperModel) + rustBiceps
        .map {
            File(rootProject.projectDir, it).listFiles()?.toList() ?: emptyList()
        }.flatten().toSet().also { files ->
            project.logger.info("Removing generator output: ${files.joinToString { it.absolutePath }}")
            delete = files
        }
}

// attach to normal gradle tasks
tasks.assemble { dependsOn(executeConverterTask, executeConverterExtensionsTask, copyTask) }