#!/bin/bash

base_version=$(cat config/base_version.txt)

if [[ -z "${CI_PIPELINE_IID}" ]]; then
  version="${base_version}"
else
  version="${base_version}-${CI_PIPELINE_IID}"
fi

echo "Using target version $version"

if [ -f "/etc/arch-release" ]; then
  pacman -S --noconfirm wget npm || exit 1
fi

echo "Clearing previous data"
rm -rf javascript/src/*

# setup buf
cd javascript
npm version "${version}"
npm install

# generate model
npx buf generate ../proto

## upload
# deploy wheel only if enabled
if [[ "$*" == *--deploy* ]]
then
npm publish
fi