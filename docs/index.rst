.. protoSDC model documentation master file, created by
   sphinx-quickstart on Wed Dec 22 15:38:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to protoSDC model's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   model/definitions.rst
   model/discovery.rst
   model/metadata.rst
   model/biceps_services.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. include:: ../README.rst