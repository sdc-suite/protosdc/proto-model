sphinx==7.2.6
myst-parser==2.0.0
sphinxcontrib-mermaid==0.9.2
sphinx_rtd_theme==2.0.0
sphinxcontrib_applehelp==1.0.8
attrs