###############
BICEPS Services
###############

BICEPS comes with a handful of pre-defined services that are bound to gRPC as described in the following sections.

Message exchange patterns
#########################

gRPC supports four different kinds of remote procedure call parametrization:

- Unary RPC: Simple request response
- Client streaming RPC: The client sends a stream of messages, the server responds with a single message
- Server streaming RPC: The client sends a single message, the server responds with a stream of messages
- Bidirectional streaming RPC: Initiated by the client, both parties exchange a stream of messages

BICEPS on the contrary differentiates between the following message exchange patterns (MEP):

- Request Response
- Notification
- Stream

In protoSDC, the BICEPS Request Response MEP is mapped to gRPC's Unary RPC whereas Notification and Stream are conceptually resembling and hence mapped to gRPC's Server streaming RPC.

Service types
#############

protoSDC discovery uses service types to communicate which services are implemented by an :term:`Endpoint`. The
following list reflects the currently available service types.

- Get Service: `org.somda.protosdc.service.Get`
- Set Service: `org.somda.protosdc.service.Set`
- MDIB Reporting Service: `org.somda.protosdc.service.MdibReporting`

.. note::
    - protoSDC specifies a slightly different but functionally equivalent service interface than the one that is specified in BICEPS
    - The containment tree service, archive service and localization service have not been specified yet

Service mappings
################

Get Service
***********

TBD

Set Service
***********

TBD

MDIB Reporting Service
**********************

TBD
