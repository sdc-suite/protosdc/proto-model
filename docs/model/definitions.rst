.. _definitions:

###########
Definitions
###########

.. glossary::

    Endpoint
      A network peer in an IP-based IT network. It is accessible at one or more physical addresses.

    Endpoint identifier
      A stable, globally unique identifier of an *Endpoint* that is constant across re-initializations of the *Endpoint*, and constant across network interfaces and IPv4/v6.

    Service Provider
      An :term:`Endpoint` that makes itself available for discovery.

    Service Consumer
      An :term:`Endpoint` that searches for :term:`Service Providers<Service Provider>`.

    Discovery Proxy
      An :term:`Endpoint` that facilitates discovery of :term:`Service Providers<Service Provider>` by :term:`Service Consumers<Service Consumer>`.

    Scope
      A message attribute to be used by :term:`Service Providers<Service Provider>` to be organized into logical groups.

    Hello
      A message sent by a :term:`Service Provider` when it joins a network; this message contains key information for the :term:`Service Consumer`.

    Bye
      A message sent by a :term:`Service Provider` when it leaves a network.

    SearchRequest
      A message sent by a :term:`Service Consumer` searching for :term:`Service Providers<Service Provider>` by :term:`Scopes<Scope>` or :term:`Endpoint Identifier`.

    SearchResponse
      A message sent by a :term:`Service Provider` in response to a :term:`SearchRequest`.

    Metadata
      Information about a :term:`Service Provider`; includes, but is not limited to, human-readable names, model names, firmware version, and serial number.

    Ad hoc Mode
        An operational mode of discovery in which the :term:`Hello`, :term:`Bye` and :term:`SearchRequest` messages are sent by using UDP/IP multicast.

    Managed Mode
        An operational mode of discovery in which the :term:`Hello`, :term:`Bye` and :term:`SearchRequest` messages are sent unicast to a :term:`Discovery Proxy`.
