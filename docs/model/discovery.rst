#########
Discovery
#########

protoSDC discovery defines a discovery protocol to locate gRPC services in a protoSDC-featured IT network. It is based on WS-Discovery to the extent that it functionally covers aspects like probe filtering, endpoint resolution, and a decentralized :term:`Ad hoc Mode` as well as a centralized :term:`Managed Mode`.

However, protoSDC discovery is not based on talkative XML messages but on protobuf-encoded messages. Moreover, it comes with a reduced set of message types: :term:`Hello`, :term:`Bye`, :term:`SearchRequest`, :term:`SearchResponse`.

See :ref:`definitions` for more information on used terms.

Address information
###################

In :term:`Ad hoc Mode`, protoSDC discovery leverages the following address information to exchange messages:

- Port: 6464 [`IANA <https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=6464>`_]
- IPv4 multicast address: 239.255.255.250
- IPv6 multicast address: FF02::C (link-local scope)

Action identifiers
##################

Although protobuf messages are dispatched by means of the `OneOf <https://developers.google.com/protocol-buffers/docs/proto3#oneof>`_ directive, protoSDC discovery defines the following action identifiers to facilitate debugging and additional message validation.

- :term:`Hello`: `org.somda.protosdc.discovery.action.Hello`
- :term:`Bye`: `org.somda.protosdc.discovery.action.Bye`
- :term:`SearchRequest`: `org.somda.protosdc.discovery.action.SearchRequest`
- :term:`SearchResponse`: `org.somda.protosdc.discovery.action.SearchResponse`

Implicit discovery in Ad hoc Mode
#################################

In :term:`Ad hoc Mode`, a :term:`Service Provider` shall send a :term:`Hello` when it joins the network, when its :term:`Endpoint` information changes, or when its :term:`Metadata` changes. A :term:`Service Consumer` that is interested in the :term:`Service Provider` may send a :term:`SearchRequest` if the :term:`Hello` does not contain a physical address.

.. note::
    A physical address can be missing from a :term:`Hello`, e.g. if the :term:`Service Provider` multicasts to multiple network adapters.

A :term:`Service Provider` shall not send a :term:`Bye` as the :term:`Ad hoc Mode` uses an unsecured channel, and most :term:`Service Consumers<Service Consumer>` would ignore that message anyway.

.. mermaid::

    sequenceDiagram
        Service Provider->>Service Consumer: Hello
        opt if physical addresses are missing
            Service Consumer->>Service Provider: SearchRequest
            Service Provider->>Service Consumer: SearchResponse
        end

Explicit discovery in Ad hoc Mode
#################################

In :term:`Ad hoc Mode`, a :term:`Service Consumer` may send a :term:`SearchRequest` to search for :term:`Service Providers<Service Provider>`.

.. mermaid::

    sequenceDiagram
        Service Consumer->>Service Provider: SearchRequest
        Service Provider->>Service Consumer: SearchResponse

Implicit discovery in Managed Mode
##################################

In :term:`Managed Mode`, a :term:`Service Consumer` may use the `ConsumerProxyService` of a :term:`Discovery Proxy` to subscribe to :term:`Hello` and :term:`Bye`. The :term:`Discovery Proxy` shall respond with a stream of `HelloAndByeResponse` messages.

.. note::

    * `ConsumerProxyService` is a gRPC service
    * `ProxySubscribeHelloAndByeRequest` requests to receive a stream of `ProxyHelloAndByeResponse` messages
    * `ProxyHelloAndByeResponse` is used by the `ConsumerProxyService` to convey :term:`Hello` and :term:`Bye`

.. mermaid::

    sequenceDiagram
        Service Consumer->>Discovery Proxy: ConsumerProxyService.SubscribeHelloAndBye(ProxySubscribeHelloAndByeRequest)
        Discovery Proxy-->>Service Consumer: ProxySubscribeHelloAndByeResponse
        Discovery Proxy-->>Service Consumer: ProxySubscribeHelloAndByeResponse
        Discovery Proxy-->>Service Consumer: ProxySubscribeHelloAndByeResponse

Explicit discovery in Managed Mode
##################################

In :term:`Managed Mode`, a :term:`Service Consumer` may use the `ConsumerProxyService` of a :term:`Discovery Proxy` to search for :term:`Service Providers<Service Provider>` matching specific :term:`Scopes<Scope>` or :term:`Endpoint identifier`.

.. note::

    * `ConsumerProxyService` is a gRPC service
    * `ProxySearchRequest` and `ProxySearchResponse` are messages used by the `Discovery Proxy` to convey search request and response data

.. mermaid::

    sequenceDiagram
        Service Consumer->>+Discovery Proxy: ConsumerProxyService.Search(ProxySearchRequest)
        Discovery Proxy-->>-Service Consumer: ProxySearchResponse

Notification of Hello and Bye in Managed Mode
#############################################

In :term:`Managed Mode`, the :term:`Service Provider` shall use the `ProviderProxyService` of a :term:`Discovery Proxy` to establish a stream of `NotifyHelloAndByeRequest` messages.

.. note::

    * `ProviderProxyService` is a gRPC service
    * `ProxyNotifyHelloAndByeRequest` is used by the `ProviderProxyService` to publish :term:`Hello` and :term:`Bye`

.. mermaid::

    sequenceDiagram
        Service Provider->>+Discovery Proxy: ProviderProxyService.NotifyHelloAndBye(ProxyNotifyHelloAndByeRequest)
        Service Provider->>+Discovery Proxy: ProviderProxyService.NotifyHelloAndBye(ProxyNotifyHelloAndByeRequest)
        Service Provider->>+Discovery Proxy: ProviderProxyService.NotifyHelloAndBye(ProxyNotifyHelloAndByeRequest)
