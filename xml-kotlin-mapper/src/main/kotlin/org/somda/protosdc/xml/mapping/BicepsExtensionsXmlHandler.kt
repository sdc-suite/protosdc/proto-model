package org.somda.protosdc.xml.mapping

import org.w3c.dom.Document
import org.w3c.dom.Node

interface BicepsExtensionsXmlHandler {
    fun namespace(): String
    fun mapKnownToKotlin(source: Node): Any?
    fun mapKnownToXml(source: Any, targetOwner: Document): Node?
}