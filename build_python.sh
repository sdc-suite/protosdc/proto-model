#!/bin/bash

protocversionfile="config/protobuf_version.txt"
grpcversionfile="config/grpc_version.txt"
source "$protocversionfile"
source "$grpcversionfile"

echo "Using protoc version $protoc_version"
echo "Using grpc version $grpc_python_version"

pacman -S --noconfirm python-setuptools python-wheel twine python-pip wget || exit 1

# download python protoc
mkdir protoc
protoc_url="https://github.com/protocolbuffers/protobuf/releases/download/v$protoc_version/protoc-$protoc_version-linux-x86_64.zip"
echo "Downloading proto from $protoc_url into ./protoc"
wget -qO- "$protoc_url" | bsdtar -xvf- -C protoc
chmod a+x protoc/bin/protoc

## install correct grpc version
echo "Installing grpc in version $grpc_python_version"
python -m pip install --break-system-packages "grpcio==$grpc_python_version" "grpcio-tools==$grpc_python_version"  || exit 5

echo "Clearing previous data"
rm -rf python/src/*
rm -rf python/build
rm -rf python/dist

# generate model and service data
echo "Generating model and service data"
protoc/bin/protoc -I=proto --python_out=python/src $(find proto -name '*.proto' -follow ! \( -name 'sdc_services.proto' -o -name 'metadata_services.proto' \)) || exit 2

# generate services
echo "Generating services"
python -m grpc_tools.protoc -Iproto --python_out=python/src --grpc_python_out=python/src proto/org/somda/protosdc/proto/model/sdc_services.proto proto/org/somda/protosdc/proto/model/metadata/metadata_services.proto || exit 3

# build wheel
cd python || exit 4
python setup.py sdist bdist_wheel

# deploy wheel only if enabled
if [[ "$*" == *--deploy* ]]
then
TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token twine upload --repository-url https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi dist/*
fi