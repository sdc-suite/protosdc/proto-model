Introduction
==================

Being a part of the IEEE 11073 SDC standards series, the Medical Devices Profile for Web Services aka MDPWS is used to
be the vehicle to convey data between medical devices. However, MDPWS is based on stale standards like HTTP 1.1, Web
Services and most prominently XML.

This results in a variety of different challenges which were the driving force to try out something new, i.e. coming up
with an approach that references less standards, provides efficient serialization and implements publish-subscribe
capabilities off the shelf. This lead to protoSDC which is based on gRPC, a modern open source high performance Remote
Procedure Call (RPC) framework that leverages protobuf, Google's language-neutral, platform-neutral, and extensible answer
to XML. protoSDC provides a full-fledged transport layer definition for the Basic Integrated Clinical Environment Protocol
Specification (BICEPS).

protoSDC model
--------------

In this project, you will find:

* Conversion of XML Schema to
    * protobuf message structures
    * kotlin data classes
    * rust structs
* BICEPS services modelled as gRPC services implementing Request-response and notification message exchange patterns
* Implicit and explicit endpoint discovery based on either ad-hoc (multicast) or managed (discovery proxy) mode

For more information visit https://model.protosdc.org