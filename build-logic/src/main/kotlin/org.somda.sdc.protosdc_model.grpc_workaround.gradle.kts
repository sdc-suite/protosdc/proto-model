import org.gradle.api.JavaVersion

plugins {
    java
    id("com.google.osdetector")
}

// workaround for "protoc-gen-grpc-java not available on apple m1"
// see: https://github.com/grpc/grpc-java/issues/7690
val osClassification by extra(
    when (val it = osdetector.classifier) {
        "osx-aarch_64" -> "osx-x86_64"
        else -> it
    }
)

dependencies {
    if (JavaVersion.current().isJava9Compatible) {
        // workaround for "Java 9: cannot find symbol javax.annotation.Generated"
        // see: https://github.com/grpc/grpc-java/issues/3633
        implementation(libs.javax.annotation)
    }
}
