plugins {
    idea
    java
    `java-library`

    id("org.somda.repository_collection")
}

group = "org.somda.sdc.protosdc"
val baseVersion = File(rootProject.projectDir, "./config/base_version.txt").readText()
val isSnapshot: Boolean = hasProperty("snapshot")
val buildId: String? = System.getenv("CI_PIPELINE_IID")

version = when (isSnapshot) {
    true -> "$baseVersion-SNAPSHOT" + (buildId?.let { "+$buildId" } ?: "")
    false -> baseVersion
}

if (baseVersion != version) {
    logger.warn(
        "Changed version from '$baseVersion' to '$version'"
    )
} else {
    logger.warn(
        "Keep version '$baseVersion'"
    )
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))

    implementation(libs.bundles.logging)
    testImplementation(libs.bundles.testing)
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

