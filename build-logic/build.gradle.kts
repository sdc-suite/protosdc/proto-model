plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/60445622/packages/maven")
}

dependencies {
    implementation(files(libs.javaClass.superclass.protectionDomain.codeSource.location))
    implementation(libs.gradleplugins.kotlin.jvm)
    implementation(libs.gradleplugins.somda.repository.collection)
    implementation(libs.gradleplugins.google.protobuf)
    implementation(libs.gradleplugins.google.osdetector)
}
