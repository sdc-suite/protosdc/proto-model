use protosdc_mapping::{MappingError, ProtoSdcType};
use std::ops::Deref;

/// Wrap decimal to allow implementing the mapping trait.
#[derive(Clone, Debug, PartialEq, Default)]
pub struct ProtoDecimal {
    decimal: rust_decimal::Decimal,
}

impl ProtoSdcType<protosdc_proto::common::Decimal> for ProtoDecimal {}

impl Deref for ProtoDecimal {
    type Target = rust_decimal::Decimal;

    fn deref(&self) -> &Self::Target {
        &self.decimal
    }
}

impl TryFrom<protosdc_proto::common::Decimal> for ProtoDecimal {
    type Error = MappingError;

    fn try_from(value: protosdc_proto::common::Decimal) -> Result<Self, Self::Error> {
        Ok(Self {
            decimal: rust_decimal::Decimal::new(value.value, value.scale),
        })
    }
}

impl TryFrom<String> for ProtoDecimal {
    type Error = MappingError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Ok(Self {
            decimal: rust_decimal::Decimal::from_str_exact(&value).map_err(|err| {
                Self::Error::FromProtoGeneric {
                    message: format!("Error: {}\ninput: {}", err, value),
                }
            })?,
        })
    }
}

impl From<i32> for ProtoDecimal {
    fn from(value: i32) -> Self {
        Self {
            decimal: rust_decimal::Decimal::from(value),
        }
    }
}

impl From<i64> for ProtoDecimal {
    fn from(value: i64) -> Self {
        Self {
            decimal: rust_decimal::Decimal::from(value),
        }
    }
}

impl From<rust_decimal::Decimal> for ProtoDecimal {
    fn from(value: rust_decimal::Decimal) -> Self {
        Self { decimal: value }
    }
}

#[allow(clippy::from_over_into)]
impl Into<protosdc_proto::common::Decimal> for ProtoDecimal {
    fn into(self) -> protosdc_proto::common::Decimal {
        protosdc_proto::common::Decimal {
            value: self
                .decimal
                .mantissa()
                .try_into()
                .expect("Could not fit mantissa into i64"),
            scale: self.decimal.scale(),
        }
    }
}
