use crate::types::dates_common::{
    parse_i32, parse_u32, DATE_PARSER, DAY_GROUP, MONTH_GROUP, TZ_HOURS_GROUP, TZ_MINUTES_GROUP,
    TZ_SIGN_GROUP, TZ_UTC_GROUP, YEAR_GROUP, YEAR_SIGN_GROUP,
};
use chrono::{Date, Datelike, FixedOffset, NaiveDate, Utc};
use protosdc_mapping::{MappingError, ProtoSdcType};

#[derive(Debug, Clone, PartialEq)]
pub enum DateEnum {
    // when we received a timezone, converted to UTC
    Date(Date<Utc>),
    // no timezone info present
    NaiveDate(NaiveDate),
}

#[derive(Debug, Clone, PartialEq)]
pub struct ProtoDate {
    pub date: DateEnum,
}

impl ProtoSdcType<String> for ProtoDate {}

fn parse_date_regex(value: &str) -> Result<DateEnum, MappingError> {
    let regex_parsed = DATE_PARSER.captures(value);

    match regex_parsed {
        None => Err(MappingError::FromProtoGeneric {
            message: "date did not pass validation".to_string(),
        }),
        Some(capture) => {
            let year_sign = capture.name(YEAR_SIGN_GROUP);
            let year: i32 = parse_i32(&capture.name(YEAR_GROUP), "date", "year")?;
            let month: u32 = parse_u32(&capture.name(MONTH_GROUP), "date", "month")?;
            let day: u32 = parse_u32(&capture.name(DAY_GROUP), "date", "day")?;

            let naive_date = match year_sign {
                Some(_) => NaiveDate::from_ymd_opt(-year, month, day).ok_or(
                    MappingError::FromProtoGeneric {
                        message: "date did not pass validation".to_string(),
                    },
                )?,
                None => NaiveDate::from_ymd_opt(year, month, day).ok_or(
                    MappingError::FromProtoGeneric {
                        message: "date did not pass validation".to_string(),
                    },
                )?,
            };

            let tz_sign_raw = capture.name(TZ_SIGN_GROUP);
            let tz_hours_raw = capture.name(TZ_HOURS_GROUP);
            let tz_minutes_raw = capture.name(TZ_MINUTES_GROUP);
            let tz_utc_raw = capture.name(TZ_UTC_GROUP);

            if let (Some(tz_sign_match), Some(tz_hours_match), Some(tz_minutes_match)) =
                (tz_sign_raw, tz_hours_raw, tz_minutes_raw)
            {
                let tz_hours: i32 = tz_hours_match.as_str().parse().map_err(|_| {
                    MappingError::FromProtoGeneric {
                        message: "hours could not be parsed to i32".to_string(),
                    }
                })?;
                let tz_minutes: i32 = tz_minutes_match.as_str().parse().map_err(|_| {
                    MappingError::FromProtoGeneric {
                        message: "minutes could not be parsed to i32".to_string(),
                    }
                })?;

                let parsed_tz_opt: Option<FixedOffset> = match tz_sign_match.as_str() {
                    // correct time by adding the difference to utc
                    "-" => FixedOffset::east_opt(tz_hours * 3600 + tz_minutes * 60),
                    // or subtract it, if it was added before
                    _ => FixedOffset::east_opt(-(tz_hours * 3600 + tz_minutes * 60)),
                };

                let parsed_tz = if let Some(it) = parsed_tz_opt {
                    it
                } else {
                    return Err(MappingError::FromProtoGeneric {
                        message: "Parsed timezone was out of range".to_string(),
                    });
                };

                let date: Date<FixedOffset> = Date::from_utc(naive_date, parsed_tz);
                let utc_date: Date<Utc> = Date::from_utc(date.naive_utc(), Utc);

                Ok(DateEnum::Date(utc_date))
            } else if tz_utc_raw.is_some() {
                Ok(DateEnum::Date(Date::from_utc(naive_date, Utc)))
            } else {
                Ok(DateEnum::NaiveDate(naive_date))
            }
        }
    }
}

impl TryFrom<String> for ProtoDate {
    type Error = MappingError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Ok(Self {
            date: parse_date_regex(&value)?,
        })
    }
}

impl Into<String> for ProtoDate {
    fn into(self) -> String {
        match self.date {
            DateEnum::Date(date) => format!("{}-{}-{}Z", date.year(), date.month(), date.day()),
            DateEnum::NaiveDate(date) => date.to_string(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::types::date::{DateEnum, MappingError, ProtoDate};
    use chrono::{NaiveDate, TimeZone, Utc};

    #[test]
    fn parse_valid_dates() -> anyhow::Result<()> {
        let valid = vec![
            (
                DateEnum::NaiveDate(NaiveDate::from_ymd_opt(2001, 10, 26).unwrap()),
                "2001-10-26",
                "2001-10-26",
            ),
            (
                DateEnum::Date(Utc.ymd_opt(2001, 10, 26).unwrap()),
                "2001-10-26+02:00",
                "2001-10-26Z",
            ),
            (
                DateEnum::Date(Utc.ymd_opt(2001, 10, 26).unwrap()),
                "2001-10-26Z",
                "2001-10-26Z",
            ),
            (
                DateEnum::Date(Utc.ymd_opt(2001, 10, 26).unwrap()),
                "2001-10-26+00:00",
                "2001-10-26Z",
            ),
            (
                DateEnum::NaiveDate(NaiveDate::from_ymd_opt(-2001, 10, 26).unwrap()),
                "-2001-10-26",
                "-2001-10-26",
            ),
            (
                DateEnum::NaiveDate(NaiveDate::from_ymd_opt(-20000, 04, 01).unwrap()),
                "-20000-04-01",
                "-20000-04-01",
            ),
        ];

        for entry in valid {
            let proto_date: ProtoDate = entry
                .1
                .to_string()
                .try_into()
                .unwrap_or_else(|_err| panic!("Could not parse {}", entry.1));
            assert_eq!(entry.0, proto_date.date);

            // check string representation also matches
            let proto_date_str: String = proto_date.into();
            assert_eq!(entry.2, &proto_date_str);
        }

        Ok(())
    }

    #[test]
    fn parse_invalid_date() -> anyhow::Result<()> {
        let invalid_strings = vec!["2020-04-04ZZ", "", "13-11-28", "13/28/11", "BerndDasBrot"];

        for invalid_str in invalid_strings {
            let parsed: Result<ProtoDate, MappingError> = invalid_str.to_string().try_into();
            assert!(parsed.is_err())
        }

        Ok(())
    }
}
