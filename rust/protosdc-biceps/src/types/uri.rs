use protosdc_mapping::{MappingError, ProtoSdcType};
use std::ops::Deref;

#[derive(Debug, Clone, PartialEq)]
pub struct ProtoUri {
    pub uri: String,
}

impl Deref for ProtoUri {
    type Target = str;
    fn deref(&self) -> &Self::Target {
        &self.uri
    }
}

impl TryFrom<String> for ProtoUri {
    type Error = MappingError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Ok(Self { uri: value })
    }
}

impl Into<String> for ProtoUri {
    fn into(self) -> String {
        self.uri.to_string()
    }
}

impl ProtoSdcType<String> for ProtoUri {}

#[cfg(test)]
mod test {
    use crate::types::ProtoUri;
    use anyhow::Result;

    #[test]
    fn parsing_test() -> Result<()> {
        let valid_uris = vec![
            "http://example\t.\norg",
            "https://test:@test",
            "ftp://example///////////",
        ];

        for uri in valid_uris {
            ProtoUri::try_from(uri.to_string())?;
        }

        Ok(())
    }
}
