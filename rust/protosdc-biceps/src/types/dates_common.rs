use const_format::concatcp;
use lazy_static::lazy_static;
use protosdc_mapping::MappingError;
use regex::{Match, Regex};

pub(crate) const YEAR_SIGN_GROUP: &str = "year_sign";
pub(crate) const YEAR_GROUP: &str = "year";
const YEAR_FRAGMENT: &str = concatcp!(
    r"(?P<",
    YEAR_SIGN_GROUP,
    r">-)?(?P<",
    YEAR_GROUP,
    r">[1-9][0-9]{3,}|0[0-9]{3})"
);

pub(crate) const MONTH_GROUP: &str = "month";
const MONTH_FRAGMENT: &str = concatcp!(r"(?P<", MONTH_GROUP, r">0[1-9]|1[0-2])");

pub(crate) const DAY_GROUP: &str = "day";
const DAY_FRAGMENT: &str = concatcp!(r"(?P<", DAY_GROUP, r">0[1-9]|[12][0-9]|3[01])");

pub(crate) const HOUR_GROUP: &str = "hour";
pub(crate) const MINUTE_GROUP: &str = "minute";
pub(crate) const SECOND_GROUP: &str = "second";
pub(crate) const MILLISECOND_GROUP: &str = "millisecond";

const TIME_FRAGMENT: &str = concatcp!(
    r"((?P<",
    HOUR_GROUP,
    r">[01][0-9]|2[0-3]):(?P<",
    MINUTE_GROUP,
    r">[0-5][0-9]):((?P<",
    SECOND_GROUP,
    r">([0-5][0-9]|60))(\.(?P<",
    MILLISECOND_GROUP,
    r">\d+))?|(24:00:00(\.0+)?)))"
);

pub(crate) const TZ_SIGN_GROUP: &str = "tz_sign";
pub(crate) const TZ_HOURS_GROUP: &str = "tz_hours";
pub(crate) const TZ_MINUTES_GROUP: &str = "tz_minutes";
pub(crate) const TZ_UTC_GROUP: &str = "tz_utz";
const TZ_FRAGMENT: &str = concatcp!(
    r"((?P<",
    TZ_SIGN_GROUP,
    r">[+-])(?P<",
    TZ_HOURS_GROUP,
    r">\d{2}):(?P<",
    TZ_MINUTES_GROUP,
    r">\d{2}))|(?P<",
    TZ_UTC_GROUP,
    r">Z)"
);
const TZ_FRAGMENT_OPT: &str = concatcp!(r"(", TZ_FRAGMENT, r")?");

const YEAR_MONTH_EXPR: &str = concatcp!(
    r"^",
    YEAR_FRAGMENT,
    r"-",
    MONTH_FRAGMENT,
    TZ_FRAGMENT_OPT,
    r"$"
);
const YEAR_EXPR: &str = concatcp!(r"^", YEAR_FRAGMENT, TZ_FRAGMENT_OPT, r"$");
const DATE_EXPR: &str = concatcp!(
    r"^",
    YEAR_FRAGMENT,
    r"-",
    MONTH_FRAGMENT,
    r"-",
    DAY_FRAGMENT,
    TZ_FRAGMENT_OPT,
    r"$"
);

const DATE_TIME_EXPR: &str = concatcp!(
    r"^",
    YEAR_FRAGMENT,
    r"-",
    MONTH_FRAGMENT,
    r"-",
    DAY_FRAGMENT,
    "T",
    TIME_FRAGMENT,
    TZ_FRAGMENT_OPT,
    r"$"
);

lazy_static! {
    pub(crate) static ref YEAR_MONTH_PARSER: Regex = Regex::new(YEAR_MONTH_EXPR).unwrap();
    pub(crate) static ref YEAR_PARSER: Regex = Regex::new(YEAR_EXPR).unwrap();
    pub(crate) static ref DATE_PARSER: Regex = Regex::new(DATE_EXPR).unwrap();
    pub(crate) static ref DATE_TIME_PARSER: Regex = Regex::new(DATE_TIME_EXPR).unwrap();
}

pub(crate) fn parse_i32(
    opt: &Option<Match>,
    calling_type_name: &str,
    element_name: &str,
) -> Result<i32, MappingError> {
    opt.ok_or(MappingError::FromProtoGeneric {
        message: format!("{} did not contain {}", calling_type_name, element_name),
    })?
    .as_str()
    .parse()
    .map_err(|_| MappingError::FromProtoGeneric {
        message: format!(
            "{} in {} could not be parsed to i32",
            element_name, calling_type_name
        ),
    })
}

pub(crate) fn parse_u32(
    opt: &Option<Match>,
    calling_type_name: &str,
    element_name: &str,
) -> Result<u32, MappingError> {
    opt.ok_or(MappingError::FromProtoGeneric {
        message: format!("{} did not contain {}", calling_type_name, element_name),
    })?
    .as_str()
    .parse()
    .map_err(|_| MappingError::FromProtoGeneric {
        message: format!(
            "{} in {} could not be parsed to u32",
            element_name, calling_type_name
        ),
    })
}
