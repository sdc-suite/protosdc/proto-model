use protosdc_mapping::{MappingError, ProtoSdcType};
use std::ops::Deref;
use std::time::Duration;

/// Wrap duration to allow implementing the mapping trait.
#[derive(Clone, Debug, PartialEq, Default)]
pub struct ProtoDuration {
    duration: Duration,
}

impl ProtoSdcType<prost_types::Duration> for ProtoDuration {}

impl Deref for ProtoDuration {
    type Target = Duration;

    fn deref(&self) -> &Self::Target {
        &self.duration
    }
}

impl TryFrom<prost_types::Duration> for ProtoDuration {
    type Error = MappingError;

    fn try_from(value: prost_types::Duration) -> Result<Self, Self::Error> {
        Ok(Self {
            duration: Duration::new(value.seconds as u64, value.nanos as u32),
        })
    }
}

#[allow(clippy::from_over_into)]
impl Into<prost_types::Duration> for ProtoDuration {
    fn into(self) -> prost_types::Duration {
        prost_types::Duration {
            seconds: self.as_secs() as i64,
            nanos: self.subsec_nanos() as i32,
        }
    }
}

impl From<::std::time::Duration> for ProtoDuration {
    fn from(value: Duration) -> Self {
        Self { duration: value }
    }
}

impl Into<::std::time::Duration> for ProtoDuration {
    fn into(self) -> ::std::time::Duration {
        self.duration
    }
}
