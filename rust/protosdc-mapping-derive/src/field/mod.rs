use anyhow::{bail, Error};
use proc_macro2::TokenStream;
use quote::{format_ident, quote};
use syn::{Attribute, Lit, Meta, MetaList, NestedMeta, Type};

#[derive(Debug)]
pub struct Field {
    pub origin_field_name: String,
    pub proto_field_name: String,
    pub optional: bool,
    pub boxed: bool,
    pub primitive: bool,
    pub enumeration: Option<Type>,
    pub repeated: bool,
    pub custom_target: bool,
}

pub fn filter_protosdc_attributes(attributes: &[Attribute]) -> Vec<Meta> {
    attributes
        .iter()
        .flat_map(Attribute::parse_meta)
        .flat_map(|meta| match meta {
            Meta::List(MetaList { path, nested, .. }) => {
                if path.is_ident("protosdc") {
                    nested.into_iter().collect()
                } else {
                    Vec::new()
                }
            }
            _ => Vec::new(),
        })
        .flat_map(|attr| -> Result<_, _> {
            match attr {
                NestedMeta::Meta(attr) => Ok(attr),
                NestedMeta::Lit(lit) => bail!("invalid protosdc attribute: {:?}", lit),
            }
        })
        .collect()
}

impl Field {
    pub fn new(origin_field_name: String, attributes: &[Attribute]) -> Result<Option<Self>, Error> {
        let meta: Vec<Meta> = filter_protosdc_attributes(attributes);

        let mut optional = false;
        let mut boxed = false;
        let mut primitive = false;
        let mut custom_target = false;
        let mut field_name = origin_field_name.to_string();
        let mut enumeration_name = None;
        let mut repeated = false;

        for attr in meta {
            if word_attr(crate::constants::OPTIONAL, &attr) {
                optional = true
            } else if word_attr(crate::constants::BOXED, &attr) {
                boxed = true
            } else if word_attr(crate::constants::PRIMITIVE, &attr) {
                primitive = true
            } else if word_attr(crate::constants::CUSTOM_TARGET, &attr) {
                custom_target = true
            } else if word_attr(crate::constants::REPEATED, &attr) {
                repeated = true
            } else if let Ok(Some(val)) = str_attr(&attr, crate::constants::FIELD_NAME) {
                field_name = val;
            } else if let Ok(Some(val)) = str_attr(&attr, crate::constants::ENUMERATION) {
                enumeration_name = Some(syn::parse_str(&val).unwrap());
            }
        }

        Ok(Some(Self {
            origin_field_name,
            proto_field_name: field_name,
            optional,
            boxed,
            primitive,
            enumeration: enumeration_name,
            repeated,
            custom_target,
        }))
    }

    pub fn generate_to_proto(&self) -> TokenStream {
        let rust_field = format_ident!("{}", &self.origin_field_name);
        let proto_field = format_ident!("{}", &self.proto_field_name);

        match (
            self.optional,
            self.boxed,
            self.primitive,
            &self.enumeration,
            self.repeated,
            self.custom_target,
        ) {
            (true, false, true, None, false, _) => quote! {
                #proto_field: match self.#rust_field {
                    Some(x) => Some(x.into()),
                    None => None,
                }
            },
            (false, false, true, None, false, _) => quote! {
                #proto_field: self.#rust_field.into()
            },
            (false, false, false, None, false, _) => quote! {
                #proto_field: Some(self.#rust_field.into())
            },
            (true, true, false, None, false, _) => quote! {
                #proto_field: match self.#rust_field {
                    Some(val) => Some(Box::new((*val).into())),
                    None => None
                }
            },
            (false, true, false, None, false, _) => quote! {
                #proto_field: Some(Box::new((*self.#rust_field).into()))
            },
            (true, false, false, None, false, _) => quote! {
                #proto_field: match self.#rust_field {
                    Some(v) => Some(v.into()),
                    None => None,
                }
            },
            (false, false, false, Some(enm), false, _) => quote! {
                #proto_field: Into::<#enm>::into(self.#rust_field) as i32
            },
            (_, false, false, None, true, _) => quote! {
                #proto_field: self.#rust_field.into_iter().map(|it| it.into()).collect()
            },
            (_, false, true, None, true, _) => quote! {
                #proto_field: self.#rust_field.into_iter().map(|it| it.into()).collect()
            },
            _ => todo!(
                "generate_to_proto: {} {} {} {:?} {}",
                self.optional,
                self.boxed,
                self.primitive,
                self.enumeration,
                self.repeated
            ),
        }
    }

    pub fn generate_from_proto(&self) -> TokenStream {
        let proto_field = format_ident!("{}", &self.proto_field_name);
        let rust_field = format_ident!("{}", &self.origin_field_name);

        match (
            self.optional,
            self.boxed,
            self.primitive,
            &self.enumeration,
            self.repeated,
            self.custom_target,
        ) {
            (true, false, true, None, false, true) => quote! {
                #rust_field: match value.#proto_field {
                    Some(x) => Some(x.try_into()?),
                    None => None,
                }
            },
            (true, false, true, None, false, false) => quote! {
                #rust_field: match value.#proto_field {
                    Some(x) => Some(x.into()),
                    None => None,
                }
            },
            (true, false, false, None, false, _) => quote! {
                #rust_field: match value.#proto_field {
                    Some(x) => Some(x.try_into()?),
                    None => None,
                }
            },
            (true, true, false, None, false, _) => quote! {
                #rust_field: match value.#proto_field {
                    Some(x) => Some(Box::new((*x).try_into()?)),
                    None => None,
                }
            },
            (false, true, false, None, false, _) => quote! {
                #rust_field: match value.#proto_field {
                    Some(x) => Box::new((*x).try_into()?),
                    None => return Err(::protosdc_mapping::MappingError::FromProtoMandatoryMissing { field_name: stringify!(#proto_field).to_string() })
                }
            },
            (false, false, true, None, false, true) => quote! {
                #rust_field: value.#proto_field.try_into()?
            },
            (false, false, true, None, false, false) => quote! {
                #rust_field: value.#proto_field.into()
            },
            (false, false, false, None, false, _) => quote! {
                #rust_field: match value.#proto_field {
                    Some(x) => x.try_into()?,
                    None => return Err(::protosdc_mapping::MappingError::FromProtoMandatoryMissing { field_name: stringify!(#proto_field).to_string() })
                }
            },

            (false, false, false, Some(enm), false, _) => quote! {
                #rust_field: match #enm::from_i32(value.#proto_field) {
                    Some(x) => x.try_into()?,
                    None => return Err(::protosdc_mapping::MappingError::FromProtoMandatoryMissing { field_name: stringify!(#proto_field).to_string() })
                }
            },
            (_, false, false, None, true, _) => quote! {
                #rust_field: value.#proto_field.into_iter().map(|it| it.try_into()).collect::<Result<Vec<_>,_>>()?
            },
            (_, false, true, None, true, true) => quote! {
                #rust_field: value.#proto_field.into_iter().map(|it| it.try_into()).collect::<Result<Vec<_>,_>>()?
            },
            (_, false, true, None, true, false) => quote! {
                #rust_field: value.#proto_field.into_iter().map(|it| it.into()).collect::<Vec<_>>()
            },
            _ => todo!(
                "generate_from_proto: {} {} {} {:?} {}",
                self.optional,
                self.boxed,
                self.primitive,
                self.enumeration,
                self.repeated
            ),
        }
    }
}

/// Checks if an attribute matches a word.
pub(super) fn word_attr(key: &str, attr: &Meta) -> bool {
    if let Meta::Path(ref path) = *attr {
        path.is_ident(key)
    } else {
        false
    }
}

pub(super) fn str_attr(attr: &Meta, name: &str) -> Result<Option<String>, Error> {
    if !attr.path().is_ident(name) {
        return Ok(None);
    }
    match *attr {
        Meta::List(ref meta_list) => {
            // TODO(rustlang/rust#23121): slice pattern matching would make this much nicer.
            if meta_list.nested.len() == 1 {
                if let NestedMeta::Lit(Lit::Str(ref lit)) = meta_list.nested[0] {
                    return Ok(Some(lit.value()));
                }
            }
            bail!("invalid tag attribute: {:?}", attr);
        }
        Meta::NameValue(ref meta_name_value) => match meta_name_value.lit {
            Lit::Str(ref lit) => lit.value().parse::<String>().map_err(Error::from).map(Some),
            Lit::Int(ref lit) => Ok(Some(lit.base10_parse()?)),
            _ => bail!("invalid tag attribute: {:?}", attr),
        },
        _ => bail!("invalid tag attribute: {:?}", attr),
    }
}
