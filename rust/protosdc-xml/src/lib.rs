use const_format::concatcp;
use log::error;
use quick_xml::events::attributes::Attribute;
use quick_xml::events::{BytesDecl, BytesEnd, BytesStart, BytesText, Event};
use quick_xml::Writer;
use std::borrow::Cow;
use std::fmt::{Debug, Formatter};
use thiserror::Error;

mod expanded_name;
pub mod util;
mod xml_element;
pub mod xml_reader;
pub use expanded_name::ExpandedName;
pub use xml_element::{XmlElement, DO_NOT_CARE_QNAME};
pub use xml_reader::{
    ExtensionHandler, ExtensionParserError, ExtensionType, GenericXmlReaderComplexTypeRead,
    GenericXmlReaderSimpleTypeRead, XmlReader, XmlReaderComplexXmlTypeRead,
    XmlReaderSimpleXmlTypeRead,
};

const PROTOSDC_XML_VERSION: &str = env!("CARGO_PKG_VERSION");
const PROTOSDC_XML_COMMIT: &str = env!("VERGEN_GIT_SHA");
const PROTOSDC_XML_BRANCH: &str = env!("VERGEN_GIT_BRANCH");
const PROTOSDC_XML_COMMENT: &str = concatcp!(
    "generated using protosdc-xml ",
    PROTOSDC_XML_VERSION,
    " (",
    PROTOSDC_XML_COMMIT,
    ") from ",
    PROTOSDC_XML_BRANCH
);

#[derive(Debug, Error)]
pub enum ParserError {
    #[error(transparent)]
    QuickXMLError(quick_xml::Error),
    #[error(transparent)]
    ParseIntError(std::num::ParseIntError),

    #[error("Could not parse duration: {error}")]
    ParseDurationError { error: String },
    #[error("Other")]
    Other,

    #[error("Unexpected end of file")]
    UnexpectedEof,

    #[error("Unexpected parser event: {event}")]
    UnexpectedParserEvent { event: String },

    #[error("Parser reached an unexpected start event state: ({element_name}, {parser_state})")]
    UnexpectedParserStartState {
        element_name: String,
        parser_state: String,
    },

    #[error("Parser reached an unexpected end event state: ({element_name}, {parser_state})")]
    UnexpectedParserEndState {
        element_name: String,
        parser_state: String,
    },

    #[error("Parser reached an unexpected text event at state {parser_state}")]
    UnexpectedParserTextEventState { parser_state: String },

    #[error("OneOfParserFallthrough for {parser_name}")]
    OneOfParserFallthrough { parser_name: String },

    #[error("Mandatory attribute was missing: {attribute_name}")]
    MandatoryAttributeMissing { attribute_name: String },

    #[error("Mandatory element was missing: {element_name}")]
    MandatoryElementMissing { element_name: String },

    #[error("Mandatory content was missing: {element_name}")]
    MandatoryContentMissing { element_name: String },

    #[error("Element error for {element_name}")]
    ElementError { element_name: String },

    #[error(transparent)]
    MappingError(protosdc_mapping::MappingError),

    #[error(
        "Unexpected attribute encountered in {element_name}: {attribute_name}={attribute_value}"
    )]
    UnexpectedAttribute {
        element_name: String,
        attribute_name: String,
        attribute_value: String,
    },

    #[error(transparent)]
    Utf8Error(#[from] std::str::Utf8Error),

    #[error("The parser depth limit was exceeded")]
    DepthLimitExceeded,
}

#[derive(Debug, Error)]
pub enum WriterError {
    #[error(transparent)]
    QuickXmlError(#[from] quick_xml::Error),

    #[error("Illegal writer state, needed {expected}, found {actual}.")]
    IllegalState { expected: String, actual: String },

    #[error("No prefix was known or generate for {namespace}")]
    UnknownNamespace { namespace: String },

    #[error("The Start tag was already committed to the stream, could not add new attribute {key} {value}")]
    StartAlreadyCommittedAttribute { key: String, value: String },

    #[error("The namespace {namespace} was not known when writing closing tag {tag}")]
    UnknownNamespaceAtClosingTag { namespace: String, tag: String },

    #[error(transparent)]
    Utf8Error(#[from] std::str::Utf8Error),
}

pub trait SimpleXmlTypeWrite {
    fn to_xml_simple(&self, writer: &mut XmlWriter<Vec<u8>>) -> Result<Vec<u8>, WriterError>;
}
pub trait ComplexXmlTypeWrite {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError>;
}

#[derive(Debug)]
pub struct WriterStateStart<'a> {
    start: Option<BytesStart<'a>>,
}

#[derive(Debug, Default)]
pub struct WriterStateNeverStart {
    namespaces: Option<Vec<NamespaceMapping>>,
}

#[derive(Debug)]
pub enum WriterState<'a> {
    Start(WriterStateStart<'a>),
    NeverStarted(WriterStateNeverStart),
    Text,
    NotStart,
}

#[derive(Debug)]
pub struct NamespaceMapping {
    namespace: String,
    prefix: String,
}

impl NamespaceMapping {
    fn to_internal(self, level: u8) -> NamespaceMappingInternal {
        NamespaceMappingInternal {
            level,
            namespace: self.namespace,
            prefix: self.prefix,
        }
    }

    pub fn new(namespace: String, prefix: String) -> Self {
        Self { namespace, prefix }
    }
}

#[derive(Debug, Default)]
struct NamespaceMappingInternal {
    level: u8,
    namespace: String,
    prefix: String,
}

impl NamespaceMappingInternal {
    pub(crate) fn xmlns(&self) -> String {
        format!("xmlns:{}", self.prefix)
    }

    pub(crate) fn ns(&self) -> &str {
        &self.namespace
    }

    pub(crate) fn prefix(&self, tag: &str) -> String {
        format!("{}:{}", self.prefix, tag)
    }
}

pub type QNameStr = (Option<&'static str>, &'static str);
pub type QNameSlice = (quick_xml::name::ResolveResult<'static>, &'static [u8]);
pub type QNameSliceRef = (
    &'static quick_xml::name::ResolveResult<'static>,
    &'static [u8],
);

#[derive(Clone, Eq, PartialEq)]
pub struct WriterAttribute<'a> {
    /// The key to uniquely define the attribute.
    ///
    /// If [`Attributes::with_checks`] is turned off, the key might not be unique.
    pub key: quick_xml::name::QName<'a>,
    /// The raw value of the attribute.
    pub value: Cow<'a, [u8]>,
}

impl<'a> From<(&'a [u8], &'a [u8])> for WriterAttribute<'a> {
    /// Creates new attribute from raw bytes.
    /// Key is stored as-is, but the value will be escaped.
    ///
    /// # Examples
    ///
    /// ```
    /// use protosdc_xml::WriterAttribute;
    ///
    /// let features = WriterAttribute::from(("features".as_bytes(), "Bells & whistles".as_bytes()));
    /// assert_eq!(features.value, "Bells &amp; whistles".as_bytes());
    /// ```
    fn from(val: (&'a [u8], &'a [u8])) -> WriterAttribute<'a> {
        WriterAttribute {
            key: quick_xml::name::QName(val.0),
            value: match quick_xml::escape::escape(std::str::from_utf8(val.1).unwrap()) {
                Cow::Borrowed(s) => Cow::Borrowed(s.as_bytes()),
                Cow::Owned(s) => Cow::Owned(s.into_bytes()),
            },
        }
    }
}

impl<'a> From<(&'a str, &'a str)> for WriterAttribute<'a> {
    /// Creates new attribute from text representation.
    /// Key is stored as-is, but the value will be escaped.
    ///
    /// # Examples
    ///
    /// ```
    /// use protosdc_xml::WriterAttribute;
    ///
    /// let features = WriterAttribute::from(("features", "Bells & whistles"));
    /// assert_eq!(features.value, "Bells &amp; whistles".as_bytes());
    /// ```
    fn from(val: (&'a str, &'a str)) -> WriterAttribute<'a> {
        WriterAttribute {
            key: quick_xml::name::QName(val.0.as_bytes()),
            value: match quick_xml::escape::escape(val.1) {
                Cow::Borrowed(s) => Cow::Borrowed(s.as_bytes()),
                Cow::Owned(s) => Cow::Owned(s.into_bytes()),
            },
        }
    }
}

impl<'a> Into<Attribute<'a>> for WriterAttribute<'a> {
    fn into(self) -> Attribute<'a> {
        Attribute {
            key: self.key,
            value: self.value,
        }
    }
}

pub struct XmlWriter<'a, W: std::io::Write> {
    writer: Writer<W>,
    state: WriterState<'a>,
    internal: XmlWriterInternal,
}

impl<W: std::io::Write> Debug for XmlWriter<'_, W> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "XmlWriter [{:?}, {:?}]", self.state, self.internal)
    }
}

#[derive(Debug, Default)]
struct XmlWriterInternal {
    namespaces: Vec<NamespaceMappingInternal>,
    depth: u8,
    generated_namespace_counter: u8,
}

impl XmlWriterInternal {
    fn increment_depth(&mut self) {
        self.depth = self.depth + 1;
    }

    fn decrement_depth(&mut self) {
        // drop all namespaces we're losing
        let new_depth = self.depth - 1;
        let mut i = 0;
        while i < self.namespaces.len() {
            if self.namespaces[i].level >= new_depth {
                self.namespaces.remove(i);
            } else {
                i += 1;
            }
        }

        self.depth = self.depth - 1;
    }
}

impl<'x, W> XmlWriter<'x, W>
where
    W: std::io::Write,
{
    fn lookup_namespace_prefixes<'a>(
        data: &'a [NamespaceMappingInternal],
        namespace: &str,
    ) -> Option<&'a NamespaceMappingInternal> {
        data.iter().find(|it| namespace == it.namespace)
    }

    fn generate_namespace(&mut self, namespace: &str, level: u8) -> NamespaceMappingInternal {
        let new_namespace = NamespaceMappingInternal {
            level,
            namespace: namespace.to_string(),
            prefix: format!("ns{}", self.internal.generated_namespace_counter),
        };
        self.internal.generated_namespace_counter += 1;
        new_namespace
    }

    pub fn inner(&mut self) -> &mut W {
        self.writer.get_mut()
    }

    pub fn into_inner(self) -> W {
        self.writer.into_inner()
    }

    fn get_tag_to_write(
        ns_internal: &[NamespaceMappingInternal],
        namespace: Option<&str>,
        tag: &str,
    ) -> Option<String> {
        match namespace {
            None => Some(tag.to_string()),
            Some(ns) => Self::lookup_namespace_prefixes(ns_internal, ns)
                .map(|it| format!("{}:{}", it.prefix, tag)),
        }
    }

    fn write_start_never_started<'b>(
        &mut self,
        namespace: Option<&str>,
        tag: &'b str,
    ) -> Result<&mut Self, WriterError> {
        let mut ns_internal: Vec<NamespaceMappingInternal> = match &mut self.state {
            WriterState::NeverStarted(ref mut it) => it
                .namespaces
                .take()
                .unwrap()
                .into_iter()
                .map(|it| it.to_internal(0))
                .collect(),
            _ => {
                return Err(WriterError::IllegalState {
                    expected: stringify!(WriterState::NeverStarted).to_string(),
                    actual: format!("{:?}", self.state),
                })
            }
        };

        let tag_to_write: String = match Self::get_tag_to_write(&ns_internal, namespace, tag) {
            Some(it) => it,
            None => {
                let new_ns = self.generate_namespace(namespace.unwrap(), self.internal.depth);
                let tag = new_ns.prefix(tag);
                ns_internal.push(new_ns);
                tag
            }
        };

        let mut start = BytesStart::new(tag_to_write);
        for ns in ns_internal {
            start.push_attribute((ns.xmlns().as_str(), ns.ns()));
            self.internal.namespaces.push(ns);
        }
        // lookup namespace
        self.state = WriterState::Start(WriterStateStart { start: Some(start) });
        self.internal.increment_depth();
        Ok(self)
    }

    fn write_start_internal<'b>(
        &mut self,
        namespace: Option<&str>,
        tag: &'b str,
    ) -> Result<&mut Self, WriterError> {
        self.flush_start()?;
        let tag_to_write: (String, Option<NamespaceMappingInternal>) =
            self.qualify_tag(namespace, tag);

        let mut bytes_start = BytesStart::new(tag_to_write.0);
        if let Some(new_ns) = tag_to_write.1 {
            bytes_start.push_attribute((new_ns.xmlns().as_str(), new_ns.ns()));
            self.internal.namespaces.push(new_ns);
        }
        self.state = WriterState::Start(WriterStateStart {
            start: Some(bytes_start),
        });
        self.internal.increment_depth();
        Ok(self)
    }

    fn flush_start(&mut self) -> Result<(), WriterError> {
        match &mut self.state {
            WriterState::Start(ref mut it) => self
                .writer
                .write_event(Event::Start(it.start.take().unwrap()))?,
            _ => {}
        };
        Ok(())
    }

    fn write_end_internal(
        &mut self,
        namespace: Option<&str>,
        tag: &str,
    ) -> Result<&mut Self, WriterError> {
        self.flush_start()?;
        let tag_to_write = Self::get_tag_to_write(&self.internal.namespaces, namespace, tag)
            .ok_or_else(|| WriterError::UnknownNamespaceAtClosingTag {
                namespace: format!("{:?}", namespace),
                tag: tag.to_string(),
            })?;

        self.writer
            .write_event(Event::End(BytesEnd::new(tag_to_write)))?;
        self.state = WriterState::NotStart;
        self.internal.decrement_depth();
        Ok(self)
    }

    pub fn write_start<'a, 'c>(
        &mut self,
        namespace: Option<&str>,
        tag: &str,
    ) -> Result<&mut Self, WriterError> {
        match &self.state {
            WriterState::NeverStarted(_) => self.write_start_never_started(namespace, tag),
            _ => self.write_start_internal(namespace, tag),
        }
    }

    pub fn write_end(
        &mut self,
        namespace: Option<&str>,
        tag: &str,
    ) -> Result<&mut Self, WriterError> {
        self.write_end_internal(namespace, tag)
    }

    pub fn add_attribute<'b, A>(&mut self, attribute: A) -> Result<&mut Self, WriterError>
    where
        A: Into<WriterAttribute<'b>> + Debug + Clone,
    {
        match &mut self.state {
            WriterState::Start(ref mut start) => start
                .start
                .as_mut()
                .ok_or({
                    let attr: WriterAttribute = attribute.clone().into();
                    WriterError::StartAlreadyCommittedAttribute {
                        key: String::from_utf8_lossy(attr.key.0).to_string(),
                        value: String::from_utf8_lossy(&*attr.value).to_string(),
                    }
                })?
                .push_attribute(attribute.into()),
            _ => {
                let attr: WriterAttribute = attribute.into();
                return Err(WriterError::StartAlreadyCommittedAttribute {
                    key: String::from_utf8_lossy(attr.key.0).to_string(),
                    value: String::from_utf8_lossy(&*attr.value).to_string(),
                });
            }
        }
        Ok(self)
    }

    fn qualify_tag(
        &mut self,
        namespace: Option<&str>,
        tag: &str,
    ) -> (String, Option<NamespaceMappingInternal>) {
        match Self::get_tag_to_write(&self.internal.namespaces, namespace, tag) {
            Some(it) => (it, None),
            None => {
                let new_ns = self.generate_namespace(namespace.unwrap(), self.internal.depth);
                let tag = new_ns.prefix(tag);
                (tag, Some(new_ns))
            }
        }
    }

    /// Only call when having started a tag
    pub fn qualify_tag_add_ns(
        &mut self,
        namespace: Option<&str>,
        tag: &str,
    ) -> Result<String, WriterError> {
        let (tag_to_write, ns) = self.qualify_tag(namespace, tag);
        match &mut self.state {
            WriterState::Start(ref mut start_opt) => {
                if let Some(start) = &mut start_opt.start {
                    if let Some(ns_to_write) = ns {
                        start.push_attribute((ns_to_write.xmlns().as_str(), ns_to_write.ns()));
                        self.internal.namespaces.push(ns_to_write);
                    }
                    Ok(tag_to_write)
                } else {
                    Err(WriterError::StartAlreadyCommittedAttribute {
                        key: format!("xmlns:{:?}", namespace),
                        value: tag.to_string(),
                    })
                }
            }
            _ => {
                return Err(WriterError::IllegalState {
                    expected: stringify!(WriterState::Start).to_string(),
                    actual: format!("{:?}", self.state),
                })
            }
        }
    }

    pub fn add_attribute_qname(
        &mut self,
        tag_namespace: Option<&str>,
        tag: &str,
        value_namespace: Option<&str>,
        value: &str,
    ) -> Result<&mut Self, WriterError> {
        let tag_to_write: (String, Option<NamespaceMappingInternal>) =
            self.qualify_tag(tag_namespace, tag);
        // check if the new namespace matches, otherwise qualify the tag
        let value_to_write = match value_namespace {
            None => (value.to_string(), None),
            Some(value_ns) => {
                let new_is_match = match &tag_to_write.1 {
                    None => false,
                    Some(new) => new.namespace == value_ns,
                };
                if !new_is_match {
                    self.qualify_tag(Some(value_ns), value)
                } else {
                    (value.to_string(), None)
                }
            }
        };

        match &mut self.state {
            WriterState::Start(ref mut start) => {
                let start =
                    start
                        .start
                        .as_mut()
                        .ok_or(WriterError::StartAlreadyCommittedAttribute {
                            key: tag_to_write.0.clone(),
                            value: value_to_write.0.clone(),
                        })?;
                start.push_attribute((tag_to_write.0.as_str(), value_to_write.0.as_str()));
                if let Some(new_ns) = tag_to_write.1 {
                    start.push_attribute((new_ns.xmlns().as_str(), new_ns.ns()));
                    self.internal.namespaces.push(new_ns);
                };
                if let Some(new_ns) = value_to_write.1 {
                    start.push_attribute((new_ns.xmlns().as_str(), new_ns.ns()));
                    self.internal.namespaces.push(new_ns);
                }
            }
            _ => {
                return Err(WriterError::IllegalState {
                    expected: stringify!(WriterState::Start).to_string(),
                    actual: format!("{:?}", self.state),
                })
            }
        }
        Ok(self)
    }

    pub fn write_text(&mut self, text: &str) -> Result<&mut Self, WriterError> {
        self.flush_start()?;
        self.writer.write_event(Event::Text(BytesText::new(text)))?;
        self.state = WriterState::Text;
        Ok(self)
    }

    pub fn write_bytes(&mut self, vec: &[u8]) -> Result<&mut Self, WriterError> {
        self.flush_start()?;
        self.writer
            .write_event(Event::Text(BytesText::from_escaped(std::str::from_utf8(
                vec,
            )?)))?;
        self.state = WriterState::Text;
        Ok(self)
    }
}

impl<'a> XmlWriter<'a, Vec<u8>> {
    pub fn new(root_namespaces: Vec<NamespaceMapping>) -> Self {
        let buffer = vec![];
        // this is a "pretty print" writer which will probably cause errors when elements have text
        // let writer = Writer::new_with_indent(buffer, b' ', 4);
        let mut writer = Writer::new(buffer);
        if let Err(err) = init_writer(&mut writer) {
            // will probably fail later anyway, no error needed here
            error!("Could not initialize writer, error occurred: {}", err)
        };
        Self {
            writer,
            state: WriterState::NeverStarted(WriterStateNeverStart {
                namespaces: Some(root_namespaces),
            }),
            internal: XmlWriterInternal::default(),
        }
    }

    /// Initializes a new writer that does not write an xml declaration at the beginning.
    pub fn new_no_declaration(root_namespaces: Vec<NamespaceMapping>) -> Self {
        let buffer = vec![];
        // this is a "pretty print" writer which will probably cause errors when elements have text
        // let writer = Writer::new_with_indent(buffer, b' ', 4);
        let writer = Writer::new(buffer);
        Self {
            writer,
            state: WriterState::NeverStarted(WriterStateNeverStart {
                namespaces: Some(root_namespaces),
            }),
            internal: XmlWriterInternal::default(),
        }
    }
}

fn init_writer<W>(writer: &mut Writer<W>) -> Result<(), quick_xml::Error>
where
    W: std::io::Write,
{
    writer.write_event(Event::Decl(BytesDecl::new("1.0", Some("utf-8"), None)))?;
    writer.write_event(Event::Comment(BytesText::new(PROTOSDC_XML_COMMENT)))
}

#[cfg(test)]
mod test {
    use crate::{XmlWriter, PROTOSDC_XML_COMMENT};

    #[test]
    fn test_namespace_levels() -> anyhow::Result<()> {
        let mut writer = XmlWriter::new(vec![]);

        writer.write_start(Some("test"), "test_root")?;

        writer.write_start(Some("test_nested"), "test_1")?;
        writer.write_end(Some("test_nested"), "test_1")?;

        writer.write_start(Some("test_nested"), "test_2")?;
        writer.write_start(Some("test"), "test_3")?;
        writer.write_end(Some("test"), "test_3")?;
        writer.write_end(Some("test_nested"), "test_2")?;

        writer.write_end(Some("test"), "test_root")?;

        let content = std::str::from_utf8(writer.inner())?;

        let expected =
            "<?xml version=\"1.0\" encoding=\"utf-8\"?><!--".to_string() + PROTOSDC_XML_COMMENT +
"--><ns0:test_root xmlns:ns0=\"test\"><ns1:test_1 xmlns:ns1=\"test_nested\"></ns1:test_1>\
<ns2:test_2 xmlns:ns2=\"test_nested\"><ns0:test_3></ns0:test_3></ns2:test_2>\
</ns0:test_root>";

        assert_eq!(expected, content);

        Ok(())
    }

    #[test]
    fn test_namespace_attributes() -> anyhow::Result<()> {
        let mut writer = XmlWriter::new(vec![]);

        writer.write_start(Some("test"), "test_root")?;

        writer.write_start(Some("test_nested"), "test_1")?;
        writer.add_attribute_qname(
            Some("nested_key_ns"),
            "key",
            Some("nested_value_ns"),
            "value",
        )?;
        writer.add_attribute_qname(Some("nested_key_ns"), "key2", Some("test"), "value")?;

        writer.write_end(Some("test_nested"), "test_1")?;
        writer.write_end(Some("test"), "test_root")?;

        let content = std::str::from_utf8(writer.inner())?;

        let expected = "<?xml version=\"1.0\" encoding=\"utf-8\"?><!--".to_string()
            + PROTOSDC_XML_COMMENT
            + "--><ns0:test_root xmlns:ns0=\"test\">\
<ns1:test_1 xmlns:ns1=\"test_nested\" ns2:key=\"ns3:value\" xmlns:ns2=\"nested_key_ns\" \
xmlns:ns3=\"nested_value_ns\" ns2:key2=\"ns0:value\">\
</ns1:test_1>\
</ns0:test_root>";

        assert_eq!(expected, content);

        Ok(())
    }
}
