use crate::util::write_attributes;
use crate::xml_reader::{collect_attributes_reader, XmlReader, XmlReaderComplexXmlTypeRead};
use crate::{
    ComplexXmlTypeWrite, ExpandedName, ExtensionType, ParserError, QNameSlice, WriterError,
    XmlWriter,
};
use quick_xml::events::{BytesStart, Event};
use quick_xml::name::ResolveResult;
use std::collections::HashMap;
use std::io::BufRead;

pub const DO_NOT_CARE_QNAME: QNameSlice = (ResolveResult::Unbound, b"");

const XML_ELEMENT_MAX_DEPTH: usize = 10;

/// Generic XML Element Container
#[derive(Clone, Debug, PartialEq)]
pub struct XmlElement {
    pub tag: ExpandedName,
    pub attributes: HashMap<ExpandedName, String>,
    pub children: Vec<XmlElement>,
    pub text: Option<String>,
}

impl XmlElement {
    // to keep the state machine structure, there are unused assignments
    #[allow(unused_assignments)]
    fn from_xml_complex_limit_depth<B: BufRead, C: ExtensionType>(
        _tag_name: QNameSlice,
        event: &BytesStart,
        reader: &mut XmlReader<B, C>,
        depth: usize,
    ) -> Result<Self, ParserError> {
        if depth >= XML_ELEMENT_MAX_DEPTH {
            return Err(ParserError::DepthLimitExceeded);
        }

        #[derive(Clone, Copy, Debug)]
        enum State {
            Root,
            Start,
            End,
        }
        let mut state = State::Root;

        let resolved_tag = reader.resolve_element(event.name());

        let qname: ExpandedName = resolved_tag.try_into()?;

        // collect attributes
        let attributes: HashMap<ExpandedName, String> = collect_attributes_reader(event, reader)?;

        // read value
        state = State::Start;
        let mut children = vec![];
        let mut text = None;
        let mut buf = vec![];
        loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    // add more generic children we don't know
                    children.push(XmlElement::from_xml_complex_limit_depth(
                        DO_NOT_CARE_QNAME,
                        e,
                        reader,
                        depth + 1,
                    )?)
                }

                Ok((ref ns, Event::End(ref e))) => match (state, ns, e.local_name().into_inner()) {
                    (_, ns, local_name)
                        if &qname.resolve_result() == ns
                            && qname.local_name.as_bytes() == local_name =>
                    {
                        state = State::End;
                        return Ok(Self {
                            tag: qname,
                            attributes,
                            children,
                            text,
                        });
                    }
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: format!("{:?}", state),
                    })?,
                },

                Ok((_, Event::Text(ref e))) => match state {
                    State::Start => {
                        text = Some(
                            e.unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::CData(e))) => match state {
                    State::Start => {
                        text = Some(
                            // TODO: yo bro, I unescaped your text, so you can escape it and then unescape it
                            e.escape()
                                .map_err(ParserError::QuickXMLError)?
                                .unescape()
                                .map_err(ParserError::QuickXMLError)?
                                .to_string(),
                        );
                    }
                    _ => Err(ParserError::UnexpectedParserTextEventState {
                        parser_state: format!("{:?}", state),
                    })?,
                },
                Ok((_, Event::Empty(_))) => {}
                Ok((_, Event::Comment(_))) => {}
                Ok((_, Event::Decl(_))) => {}
                Ok((_, Event::PI(_))) => {}
                Ok((_, Event::DocType(_))) => {}
                Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }
}

impl XmlReaderComplexXmlTypeRead for XmlElement {
    fn from_xml_complex<B: BufRead, C: ExtensionType>(
        tag_name: QNameSlice,
        event: &BytesStart,
        reader: &mut XmlReader<B, C>,
    ) -> Result<Self, ParserError> {
        Self::from_xml_complex_limit_depth(tag_name, event, reader, 0)
    }
}

impl ComplexXmlTypeWrite for XmlElement {
    fn to_xml_complex(
        &self,
        tag_name: Option<(Option<&'static str>, &'static str)>,
        writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        let element_namespace = match tag_name {
            Some(it) => it.0,
            None => self.tag.namespace.as_deref(),
        };
        let element_tag = tag_name.map_or_else(|| self.tag.local_name.as_str(), |(_, it)| it);

        writer.write_start(element_namespace, element_tag)?;
        write_attributes(writer, &self.attributes)?;

        if let Some(text) = &self.text {
            writer.write_text(text)?;
        }

        for child in &self.children {
            child.to_xml_complex(None, writer, false)?;
        }

        writer.write_end(element_namespace, element_tag)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::xml_element::DO_NOT_CARE_QNAME;
    use crate::xml_reader::XmlReader;
    use crate::xml_reader::XmlReaderComplexXmlTypeRead;
    use crate::{find_start_element_reader, ParserError, XmlElement};

    fn recursive_count_children(element: &XmlElement) -> usize {
        assert!(element.children.len() == 1 || element.children.len() == 0);
        if let Some(child) = element.children.get(0) {
            return recursive_count_children(child) + 1;
        }
        0
    }

    #[test]
    fn test_depth() -> anyhow::Result<()> {
        let test_str_10 = "<a1><a2><a3><a4><a5><a6><a7><a8><a9><a10/></a9></a8></a7></a6></a5></a4></a3></a2></a1>";
        let test_str_11 = "<a1><a2><a3><a4><a5><a6><a7><a8><a9><a10><a11/></a10></a9></a8></a7></a6></a5></a4></a3></a2></a1>";

        {
            let mut reader = XmlReader::create(test_str_10.as_bytes());
            let mut buf = vec![];

            let element_10: XmlElement =
                find_start_element_reader!(XmlElement, DO_NOT_CARE_QNAME, reader, buf)?;

            // 9 children, 10 elements in total
            assert_eq!(9, recursive_count_children(&element_10))
        }
        {
            let mut reader = XmlReader::create(test_str_11.as_bytes());
            let mut buf = vec![];

            let element_11 = find_start_element_reader!(XmlElement, DO_NOT_CARE_QNAME, reader, buf);
            assert!(element_11.is_err());
            let element_11_err = element_11.expect_err("Not err?");
            assert!(matches!(element_11_err, ParserError::DepthLimitExceeded))
        }

        Ok(())
    }
}
